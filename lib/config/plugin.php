<?php

return array(
    'name'            => _wp('Subproducts'),
    'description'     => _wp('Generation of Subproducts'),
    'vendor'          => 'Dmitry Khaperets',
    'version'         => '1.0',
    'img'             => 'assets/img/subproducts.16.png',
    'rights'          => 1,
    'icons'           => array(
        16  => 'assets/img/subproducts.16.png',
        32  => 'assets/img/subproducts.32.png',
        64  => 'assets/img/subproducts.64.png',
        128 => 'assets/img/subproducts.128.png'
    ),
    'frontend'        => false,
    'handlers'        => array(
        'backend_menu'    => 'backendMenu',
        'category_delete' => 'categoryDelete'
    ),
    'custom_settings' => true
);
