<?php

$model = new waModel;

try {
    $model->exec('SELECT parent_id FROM shop_product');
    $model->exec('DELETE FROM shop_product WHERE parent_id IS NOT NULL');
} catch (waException $e) {
    $model->exec('ALTER TABLE shop_product ADD parent_id INT(11) NULL DEFAULT NULL AFTER id');
}

return array(
    'shop_queue'                  => array(
        'id'    => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'data'  => array('varchar', 1024, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id'
        )
    ),
    'shop_subproducts_categories' => array(
        'id'          => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'category_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'prefix'      => array('varchar', 255, 'null' => 1),
        'suffix'      => array('varchar', 255, 'null' => 1),
        ':keys'       => array(
            'PRIMARY' => 'id'
        )
    )
);
