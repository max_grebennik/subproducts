<?php

class shopQueueModel extends waModel
{

    /**
     * @var string
     */
    protected $table = 'shop_queue';

    /**
     * @var int
     */
    protected $lastId;

    /**
     * Removes and returns the first element of the list.
     *
     * @return mixed
     */
    public function pop()
    {
        $data = $this->fetch();

        $this->commit();

        return $data;
    }

    /**
     * Append the value into the end of list.
     *
     * @param mixed $value Pushes value of the list.
     *
     * @return int|true Returns id of newly added record or true (if there are no data to be inserted).
     */
    public function push($value)
    {
        return $this->insert(['data' => serialize($value)]);
    }

    /**
     * Returns the first element of the list.
     *
     * @return mixed
     */
    public function fetch()
    {
        if ($queue = $this->order('id')->limit(1)->query()->fetchAssoc()) {
            $this->lastId = $queue['id'];

            return unserialize($queue['data']);
        }
    }

    /**
     * Removes the first element of the list.
     *
     * @return bool
     */
    public function commit()
    {
        return $this->lastId && $this->deleteById($this->lastId);
    }

    /**
     * Count all elements in a list.
     *
     * @return int
     */
    public function count()
    {
        return $this->countAll();
    }

}
