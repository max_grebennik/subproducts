<?php

abstract class shopSubproductsModel extends waModel
{

    public function all()
    {
        return $this->getAll();
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function persist(array $data = array())
    {
        if ($this->hasId($data)) {
            return $this->updateById($this->getId($data), $data);
        }

        return $this->insert($data);
    }

    public function remove($id)
    {
        return $this->deleteById($id);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    protected function hasId(array $data)
    {
        return isset($data['id']) && $data['id'];
    }

    /**
     * @param array $data
     *
     * @return int|null
     */
    protected function getId(array $data)
    {
        if ($this->hasId($data)) {
            return (int) $data['id'];
        }

        return null;
    }

}
