<?php

class shopSubproductsCategoryModel extends shopSubproductsModel
{

    /**
     * @var string
     */
    protected $table = 'shop_subproducts_categories';

    /**
     * {inherotdoc}
     */
    public function persist(array $data = array())
    {
        if ($this->hasId($data) && !$data['prefix'] && !$data['suffix']) {
            return $this->remove($this->getId($data));
        }

        return parent::persist($data);
    }

    /**
     * @return array
     */
    public function current($id = null)
    {
        if (null === $id) {
            $id = SubproductHelper::getId();
        }

        return $this->getByField('category_id', $id);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function removeByCategory($id)
    {
        return $this->deleteByField('category_id', $id);
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function findProductsByCategory($id)
    {
        $dql = 'SELECT cp.product_id FROM shop_category_products cp
                LEFT JOIN shop_product p ON cp.product_id = p.id
                WHERE cp.category_id = ? AND p.status > 0';

        return $this->query($dql, $id)->fetchAll();
    }

    /**
     * @param shopPagination $paginator
     *
     * @return shopPagination
     */
    public function getSubproducts(shopPagination $paginator)
    {
        $category = SubproductHelper::getId();

        $dql = 'SELECT p.* FROM shop_category_products cp
                LEFT JOIN shop_product p ON p.id = cp.product_id
                WHERE cp.category_id = :id AND p.parent_id IS NOT NULL
                LIMIT ' . $paginator->getOffset() . ', ' . $paginator->getLimit();

        $elements = $this->query($dql, array('id' => $category))->fetchAll();

        $paginator->setElements($elements);

        return $paginator;
    }

    /**
     * @param int $page
     *
     * @return int
     */
    public function getSubproductsCount()
    {
        $category = SubproductHelper::getId();

        $dql = 'SELECT COUNT(p.id) FROM shop_category_products cp
                LEFT JOIN shop_product p ON p.id = cp.product_id
                WHERE cp.category_id = :id AND p.parent_id IS NOT NULL';

        return $this->query($dql, array('id' => $category))->fetchField();
    }

    /**
     * @param int $page
     *
     * @return shopPagination
     */
    public function paginate($page = 1)
    {
        $paginator = new shopPagination($this->getSubproductsCount(), $page);

        $this->getSubproducts($paginator);

        return $paginator;
    }

    /**
     * @param int $category
     *
     * @return array
     */
    public function getSubproductsByCategory($category = null)
    {
        if (null === $category) {
            $category = SubproductHelper::getId();
        }

        $dql = 'SELECT p.* FROM shop_category_products cp
                LEFT JOIN shop_product p ON p.id = cp.product_id
                WHERE cp.category_id = :id AND p.parent_id IS NOT NULL';

        return $this->query($dql, array('id' => $category))->fetchAll();
    }

    /**
     * @param int $category
     *
     * @return array
     */
    public function getCrushSubproducts($category)
    {
        $sql = 'UPDATE shop_category c SET c.count = (
                    SELECT COUNT(cp.product_id)
                    FROM shop_category_products cp
                    WHERE cp.category_id = c.id
                )';

        $this->query($sql);

        $dql = 'SELECT p.id FROM shop_category_products cp
                LEFT JOIN shop_product p ON p.id = cp.product_id
                WHERE (cp.category_id = :id OR p.category_id = :id)
                    AND p.id NOT IN (
                    SELECT sps.product_id FROM shop_product_skus sps
                    LEFT JOIN shop_category_products scp USING(product_id)
                    WHERE scp.category_id = :id)';

        return $this->query($dql, array('id' => $category))->fetchAll();
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function deleteSubproductsByCategory($id = null)
    {
        if (null === $id) {
            $id = SubproductHelper::getId();
        }

        $key = '%' . md5($id) . '%';
        $this->exec('DELETE FROM shop_queue WHERE data LIKE s:0', $key);

        $products = array_column($this->getSubproductsByCategory($id), 'id');

        $model = new shopProductModel;
        $model->delete($products);

        return $products;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function generate($id = null)
    {
        $settings = $this->current($id);

        if (!$settings) {
            return false;
        }
 
        $this->deleteSubproductsByCategory($settings['category_id']);

        $key = md5($settings['category_id']);
        $relations = $this->findProductsByCategory($settings['category_id']);
            foreach ($relations as $relation) {
                
                $product = new shopSubproductName($relation['product_id']);

                foreach ($product->make($settings) as $params) {


                    Queue::on(EventCreateSubproduct::class, [
                        'params' => $params,
                        'product_id' => $product['id'],
                        'category_id' => $settings['category_id'],
                        'key' => $key
                    ]);
                }
            }

        Queue::on(EventAvitoSubproducts::class);

        return true;
    }

}
