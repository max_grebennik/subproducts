<?php

class shopSubproductsPluginSettingsAction extends waViewAction
{

    public function execute()
    {
        $plugin = wa('shop')->getPlugin('subproducts');

        $settings = $plugin->getSettings();

        $this->view->assign('settings', $settings);

        $this->extendClasses();
    }

    private function extendClasses()
    {
        $shopClasses = 'wa-apps/shop/lib/classes/';
        $pluginClasses = 'wa-apps/shop/plugins/subproducts/lib/classes/';

        $originalProduct = $shopClasses . 'shopProductsCollection.class.php';
        $duplicateProduct = $pluginClasses . 'shopProductsCollectionOriginal.class.php';

        shopSubproductsPlugin::copyClass($originalProduct, $duplicateProduct);
    }

}
