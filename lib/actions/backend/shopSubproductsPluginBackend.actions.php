<?php

class shopSubproductsPluginBackendActions extends shopSubproductsPluginActions
{

    public function defaultAction()
    {
        if ($form = waRequest::post('form', [], waRequest::TYPE_ARRAY)) {
            $this->model->persist($form);
        }

        $this->view->assign('domains', wa()->getRouting()->getDomains());
        $this->view->assign('settings', $this->model->current());
        $this->view->assign('products', $this->model->paginate(waRequest::get('page')));
    }

    public function generateAction()
    {

        $this->model->generate();

        $this->redirect([
            'plugin'   => 'subproducts',
            'category' => SubproductHelper::getId()
        ]);
    }

    public function crushAction()
    {
        $model = new shopProductModel;
        $settings = $this->model->current();
        $products = $this->model->getCrushSubproducts($settings['category_id']);

        foreach ($products as $product) {
            $model->delete([$product['id']]);
        }

        $this->execute('default');
    }

    public function removeAction()
    {
        $this->model->deleteSubproductsByCategory();

        $this->execute('default');
    }

}
