<?php

class shopSubproductsPluginActions extends waViewActions
{

    /**
     * @var shopSubproductsCategoryModel
     */
    protected $model;

    /**
     * @var shopCategoryModel
     */
    protected $category;

    /**
     * @param mixed $params
     */
    public function __construct($params = null)
    {
        parent::__construct($params);

        $this->setLayout(new shopBackendLayout);

        $this->model = new shopSubproductsCategoryModel;
        $this->category = new shopCategoryModel;
    }

    protected function preExecute()
    {
        $categories = $this->category->getFullTree();
        $category = SubproductHelper::setActive($categories);

        $this->view->assign('current', $category);
        $this->view->assign('categories', $categories);
    }

    public function postExecute()
    {
        $this->view->assign('module', waRequest::get('module', 'default'));
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    protected function getSettings($name = null)
    {
        return wa()->getPlugin('subproducts')->getSettings($name);
    }

}
