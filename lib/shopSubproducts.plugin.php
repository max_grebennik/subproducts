<?php

class shopSubproductsPlugin extends shopPlugin
{

    /**
     * @return array
     */
    public function backendMenu()
    {
        // $this->uninstall();
        // $this->install();
        // $model = new waModel();
        // $model->createSchema(array());

        $plugin = waRequest::get('plugin', 'default');

        $this->getView()->assign('plugin', $plugin);

        return array('core_li' => $this->fetchView('backstage/menu-item'));
    }

    /**
     * @param array $item
     */
    public function categoryDelete($item)
    {
        if (isset($item['id'])) {
            $model = new shopSubproductsCategoryModel;
            $model->removeByCategory($item['id']);
        }
    }

    /**
     * @return waSmarty3View|waView
     */
    private function getView()
    {
        return wa()->getView();
    }

    /**
     * @param string $tpl
     *
     * @return string
     */
    private function fetchView($tpl)
    {
        return $this->getView()->fetch($this->path . "/templates/$tpl.html");
    }

    public static function copyClass($original, $duplicate)
    {
        $originalClass = static::getClassName($original);
        $duplicateClass = static::getClassName($duplicate);

        $source = file_get_contents($original);
        $source = str_replace('class ' . $originalClass, 'class ' . $duplicateClass, $source);

        return file_put_contents($duplicate, $source);
    }

    public static function getClassName($path)
    {
        $parts = explode('.', basename($path));

        return $parts[0];
    }

}
