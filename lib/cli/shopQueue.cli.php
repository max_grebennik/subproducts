<?php

/**
 * Execute the command: php cli.php shop queue
 */
class shopQueueCli extends waCliController
{

    public function __construct()
    {
        error_reporting(-1);
        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        ini_set('error_log', __DIR__ . '/errors.txt');
    }

    public function execute()
    {
        while (Proccess::canWork() && Queue::count()) {
            Queue::dispatch();
        }
    }

}
