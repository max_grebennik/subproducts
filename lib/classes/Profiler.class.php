<?php

class Profiler
{

    /**
     * @var float
     */
    private static $time;

    /**
     * @var string
     */
    private static $name;

    /**
     * @var string
     */
    private static $output;

    /**
     * @param string $name
     *
     * @return string
     */
    public static function state($name = null)
    {
        if (static::$time) {
            $time = round(microtime(true) - static::$time, 4);

            static::$output .= sprintf("%s: %f sec.\r\n", static::$name, $time);
        }

        static::$time = microtime(true);
        static::$name = $name;

        return static::$output;
    }

}
