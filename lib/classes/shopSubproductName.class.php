<?php

class shopSubproductName extends shopProduct
{

    /**
     * @param array $settings
     */
    public function make(array $settings)
    {
        $prefix = array_fill_keys(explode(' ', $settings['prefix']), null);
        $features = $this->getFeatures($settings['suffix']);
        $products = [];

        foreach ($this->getCombinations('Models') as $subproduct) {
            $subproduct['name'] = strtr($subproduct['name'], $prefix);
            $subproduct['url'] = implode(' ', [$settings['prefix'], $subproduct['name'], $features]);
            $subproduct['name'] = implode(' ', [$settings['prefix'], $subproduct['name'], $features]);

            $products[] = $subproduct;
        }

        foreach ($this->getCombinations('Part number') as $subproduct) {
            $subproduct['name'] = strtr($subproduct['name'], $prefix);
            $subproduct['url'] = implode(' ', [$subproduct['name'], $settings['prefix'], $features]);
            $subproduct['name'] = implode(' ', [$subproduct['name'], $settings['prefix'], $features]);

            $products[] = $subproduct;
        }

        return $products;
    }

    /**
     * @param string $section
     *
     * @return array
     */
    private function getCombinations($section)
    {
        $this['summary'] = strip_tags($this['summary']);
        $this['summary'] = strtr($this['summary'], ['•' => '']);
        $lines = array_filter(array_map('trim', explode("\n", $this['summary'])));
        $count = count($lines);
        $regular = '#^([a-z0-9\s\._-]+)$#i';

        for ($i = 0; $i < $count; $i++) {
            if (isset($lines[$i]) && preg_match($regular, $lines[$i]) && !in_array($lines[$i], $this->getSeries())) {
                for ($j = $i + 1; $j < $count; $j++) {
                    if (preg_match($regular, $lines[$j])) {
                        $lines[$i] .= ',' . $lines[$j];

                        unset($lines[$j]);
                    } else {
                        break;
                    }
                }
            }
        }

        $lines = array_values($lines);

        foreach ($lines as $i => $line) {
            $prefix = null;

            if (false !== strpos($line, $section) && isset($lines[$i + 1])) {
                if (preg_match('#^([a-z\s]+):?$#i', $lines[$i + 1], $matches)) {
                    $prefix = $matches[1] . ' ';
                    $i++;
                }

                preg_match_all('#([a-z0-9\s\._-]+)#i', $lines[$i + 1], $matches);

                if (count($matches[1]) <= 3) {
                    break;
                }

                $matches[1] = array_map('trim', $matches[1]);

                foreach ($matches[1] as $j => $match) {
                    $matches[1][$j] = [
                        'name' => $prefix . $match,
                        'url'  => $match
                    ];
                }

                return array_filter($matches[1]);
            }
        }

        return [];
    }

    /**
     * @param string $suffix
     *
     * @return string
     */
    public function getFeatures($suffix)
    {
        $features = [];
        $suffixes = array_map('trim', explode(',', $suffix));

        foreach ($suffixes as $name) {
            if ($feature = $this->getFeature($name)) {
                $features[] = $feature;
            }
        }

        return implode(', ', $features);
    }

    /**
     * @param string $name
     *
     * @return null|string
*/
    private function getFeature($name)
    {
        $dql = 'SELECT v.value FROM shop_feature f
                LEFT JOIN shop_product_features pf ON f.id = pf.feature_id
                LEFT JOIN shop_feature_values_varchar v ON v.feature_id = pf.feature_id
                WHERE f.name = ? AND pf.product_id = ? AND pf.feature_value_id = v.id';

        return $this->model->query($dql, $name, $this->data['id'])->fetchField();
    }

    /**
     * @return array
     */
    private function getSeries()
    {
        return [
            'ASmobile',
            'Alienware',
            'Amilo',
            'Armada',
            'Aspire One',
            'Aspire',
            'Audi',
            'Biblo',
            'Chroma',
            'ChromeBook',
            'Chromebook',
            'Compaq',
            'Diplomat',
            'DynaBook',
            'EEE PC',
            'EEEPC',
            'Easy Lite',
            'Easy Note',
            'Easy One',
            'EasyLite',
            'EasyNote',
            'EasyOne',
            'Edge',
            'Elite Book',
            'Elite',
            'EliteBook',
            'Envy Sleekbook',
            'Envy',
            'Equium',
            'Esprimo',
            'Evo',
            'Extensa',
            'Ferrari',
            'Folio',
            'Gigabeat',
            'HDX',
            'HP Compaq',
            'Helix',
            'Inspiron',
            'Jornada',
            'Lamborghini',
            'Latitude',
            'Libretto',
            'Life Book',
            'LifeBook',
            'LifeBook',
            'LiteLine',
            'Mac Book',
            'MacBook Air',
            'MacBook Pro',
            'MacBook',
            'Megabook',
            'Mini',
            'Note',
            'OmniBook',
            'One',
            'OptiPlex',
            'Pavilion',
            'Portable',
            'PowerBook',
            'Precision',
            'Presario',
            'Pro Book',
            'Pro',
            'ProBook',
            'Qosmio',
            'Revo',
            'Satellite Pro',
            'Satellite',
            'Small Business',
            'SmartPC',
            'SmartStep',
            'Special Edition',
            'Spectre',
            'Split',
            'Stream',
            'Studio',
            'Stylistic',
            'Stylistic',
            'TM TimeLineX',
            'Tablet PC',
            'Taichi',
            'Tecra',
            'Think Pad',
            'ThinkPad Edge',
            'ThinkPad Helix',
            'ThinkPad',
            'ThinkPad',
            'TouchSmart',
            'TransNote',
            'Transformer',
            'Travel Mate',
            'TravelMate',
            'Ultra book',
            'Ultrabook',
            'Vectra',
            'Vivo Book',
            'VivoBook',
            'Voodoo Envy',
            'Vostro',
            'Wind',
            'XPS',
            'Yoga',
            'ZBook',
            'Zen Book',
            'ZenBook',
            'dot',
            'iBook',
            'iGo',
            'iPower',
        ];
    }

}
