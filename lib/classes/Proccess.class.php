<?php

class Proccess
{

    /**
     * @var int
     */
    protected static $startTime;

    /**
     * @var int
     */
    protected static $maxExecutionTime;

    /**
     * @return int
     */
    public static function getExecutionTime()
    {
        if (null === static::$startTime) {
            static::$startTime = time();
            static::$maxExecutionTime = (int) ini_get('max_execution_time');
        }

        return time() - static::$startTime;
    }

    /**
     * @param int $reserveTime
     *
     * @return bool
     */
    public static function canWork($reserveTime = 10)
    {
        $executionTime = static::getExecutionTime();

        if (static::$maxExecutionTime === 0) {
            return true;
        }

        return static::$maxExecutionTime - $executionTime - $reserveTime >= 0;
    }

    /**
     * @param int $time
     *
     * @return bool
     */
    public static function wait($time = 60)
    {
        return static::getExecutionTime() <= $time;
    }

}
