<?php

class Queue
{

    /**
     * @return shopQueueModel
     */
    public static function getStorage()
    {
        static $storage = null;

        if (null === $storage) {
            $storage = new shopQueueModel;
        }

        return $storage;
    }

    /**
     * @see Queue::push()
     */
    public static function on($className, array $args = [])
    {
        return static::push(['class' => $className, 'args' => $args]);
    }

    /**
     * @return Event
     *
     * @see Queue::pop()
     */
    public static function dispatch()
    {
        $params = static::pop();

        if (class_exists($params['class'])) {
            $class = new ReflectionClass($params['class']);
            $args = isset($params['args']) ? $params['args'] : [];
            $event = $class->newInstanceArgs($args);

            return $event->dispatch();
        }
    }

    /**
     * Removes and returns the first element of the list.
     *
     * @return mixed
     */
    public static function pop()
    {
        return static::getStorage()->pop();
    }

    /**
     * Append the value into the end of list.
     *
     * @param mixed $value Pushes value of the list.
     *
     * @return int|true Returns id of newly added record or true (if there are no data to be inserted).
     */
    public static function push($value)
    {
        return static::getStorage()->push($value);
    }

    /**
     * Returns the first element of the list.
     *
     * @return mixed
     */
    public static function fetch()
    {
        return static::getStorage()->fetch();
    }

    /**
     * Removes the first element of the list.
     *
     * @return bool
     */
    public static function commit()
    {
        return static::getStorage()->commit();
    }

    /**
     * Count all elements in a list.
     *
     * @return int
     */
    public static function count()
    {
        return static::getStorage()->count();
    }

}
