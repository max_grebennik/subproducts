<?php

class shopSubproduct extends shopProduct
{

    /**
     * @param array $params
     *
     * @return false|shopProduct
     */
    public function copy(array $params)
    {
        if ($this->hasSubproduct($params['name'])) {
            return false;
        }

        $subproduct = $this->duplicate();

        $id = $subproduct['id'];
        $url = shopHelper::transliterate(preg_replace('/\s+/', '-', $params['url']));
        $skus = $subproduct['skus'];

        foreach ($skus as &$sku) {
            $sku['sku'] = trim($sku['sku']) . ' (' . $id . ')';
        }

        $subproduct['name'] = $params['name'];
        $subproduct['summary'] = $params['name'];
        $subproduct['type_id'] = $this->fetchType($this['category_id']);
        $subproduct['parent_id'] = $this['id'];
        $subproduct['url'] = $url;
        $subproduct['skus'] = $skus;

        $subproduct->save();

        $set = new shopSetProductsModel();
        $set->add(array($id), array('subproducts'));

        return $subproduct;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasSubproduct($name)
    {
        return (bool) $this->model->countByField([
                'name'      => $name,
                'parent_id' => $this['id']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function checkRights()
    {
        return true;
    }

    /**
     * @param int $category
     *
     * @return int
     */
    public function fetchType($category)
    {
        $name = $this->model->query('SELECT name FROM shop_category WHERE id = :id', [
                'id' => $category
            ])->fetchField();

        $name = _wp('Subproducts') . ': ' . $name;

        $id = $this->model->query('SELECT id FROM shop_type WHERE name = :name', [
                'name' => $name
            ])->fetchField();

        if (false === $id) {
            $id = $this->model->query('INSERT INTO shop_type SET name = :name, icon = :icon', [
                    'icon' => 'box',
                    'name' => $name
                ])->lastInsertId();
        }

        return $id;
    }

}
