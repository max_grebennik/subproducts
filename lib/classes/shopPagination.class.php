<?php

class shopPagination extends ArrayObject
{

    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $step;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var array|false
     */
    protected $pages;

    /**
     * @param int $count
     * @param int $page
     * @param int $limit
     * @param int $step
     */
    public function __construct($count = 0, $page = 1, $limit = 20, $step = 5)
    {
        $this->setPage($page);
        $this->setStep($step);
        $this->setCount($count);
        $this->setLimit($limit);
    }

    /**
     * @param array $elements The list to exchange with the current list.
     *
     * @return array The old list.
     */
    public function setElements(array $elements = [])
    {
        return $this->exchangeArray($elements);
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = abs(intval($page)) ? : 1;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $step
     */
    public function setStep($step)
    {
        $this->step = abs($step);
    }

    /**
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = abs($count);

        $this->setPage($this->page);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = abs($limit) ? : 1;

        $this->setPage($this->page);
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return ($this->page - 1) * $this->limit;
    }

    /**
     * @return bool
     */
    public function hasPages()
    {
        return count($this->getPages()) > 0;
    }

    /**
     * @return int
     */
    public function getPages()
    {
        if (false === $this->pages) {
            return [];
        } elseif (is_array($this->pages)) {
            return $this->pages;
        }

        $max = ceil($this->count / $this->limit);
        $step = $this->step;
        $page = $this->page;

        # если страниц меньше или равно удвоенному шагу - выводим их все
        if ($max <= $step * 2 + 1) {
            for ($i = 1; $i <= $max; $i++) {
                $this->pages[] = $i;
            }

            return $this->pages;
        }

        # если страниц больше, чем удвоенный шаг
        $left = $page - $step >= 1 ? intval($page - $step) : 1;
        $right = $page + $step >= $max ? $max : intval($page + $step);

        if ($left >= 2) {
            $this->pages[] = 1;

            if ($left > 2) {
                $this->pages[] = null;
            }
        }

        for ($i = $left; $i <= $right; $i++) {
            $this->pages[] = $i;
        }

        if ($right <= $max - 1) {
            if ($right < $max - 1) {
                $this->pages[] = null;
            }

            $this->pages[] = $max;
        }

        return $this->pages;
    }

}
