<?php

class SubproductHelper
{

    /**
     * @var int
     */
    private static $id;

    /**
     * @param array $categories
     *
     * @return array|null
     */
    public static function getCategory(array $categories)
    {
        $id = waRequest::get('category', 0, waRequest::TYPE_INT);

        if (!$id && $categories) {
            $category = current($categories);
        } elseif ($categories) {
            $category = $categories[$id];
        } else {
            $category = null;
        }

        return $category;
    }

    /**
     * @param array $categories
     *
     * @return array
     */
    public static function setActive(array &$categories)
    {
        $category = static::getCategory($categories);

        if ($category) {
            static::$id = $parent = $category['id'];
            $category['breadcrumbs'] = array();

            while ($parent) {
                $category['breadcrumbs'][] = $categories[$parent]['name'];

                $categories[$parent]['active'] = true;
                $parent = $categories[$parent]['parent_id'];
            }

            $category['breadcrumbs'] = array_reverse($category['breadcrumbs']);
            $category['breadcrumbs'] = implode(' > ', $category['breadcrumbs']);
        }

        return $category;
    }

    /**
     * @return int
     */
    public static function getId()
    {
        return static::$id;
    }

}
