<?php

class EventCreateSubproduct
{

    /**
     * @var array
     */
    protected $params;

    /**
     * @var shopSubproduct
     */
    protected $product;

    /**
     * @param array $params
     * @param int   $product
     */
    public function __construct(array $params, int $product)
    {
        $this->params = $params;
        $this->product = new shopSubproduct($product);
    }

    /**
     * @return false|shopProduct
     */
    public function dispatch()
    {
        return $this->product->copy($this->params);
    }

}
