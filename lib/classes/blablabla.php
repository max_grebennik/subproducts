<?php

class Bla
{

    /**
     * @param array $settings
     */
    public function make(array $settings)
    {
        $names = [];
        $prefix = array_fill_keys(explode(' ', $settings['prefix']), null);
        $features = $this->getFeatures($settings['suffix']);

        $this['summary'] = strip_tags($this['summary']);
        $this['summary'] = array_filter(array_map('trim', explode("\n", $this['summary'])));

        /*echo '<pre>';
        var_dump($this->getModels(), $this->getPartNumbers(), $this['summary']);
        echo '</pre>';
        die;   */

        foreach ($this->getCombinations('Models') as $name) {
            $name = strtr($name, $prefix);

            $names[] = implode(' ', [$settings['prefix'], $name, $features]);
        }

        foreach ($this->getCombinations('Part number') as $name) {
            $name = strtr($name, $prefix);

            $names[] = implode(' ', [$name, $settings['prefix'], $features]);
        }

        return $names;
    }

    /**
     * @return array
     */
    private function getModels()
    {
        $lines = $this->getCombinations('Models');

        return $lines;
    }

    /**
     * @return array
     */
    private function getPartNumbers()
    {
        $lines = $this->getCombinations('Part number');

        return $lines;
    }

    /**
     * @param string $section
     *
     * @return array
     */
    private function getCombinations($section)
    {
        $lines = [];
        $count = count($this['summary']);

        foreach ($this['summary'] as $i => $line) {
            if (false !== strpos($line, $section)) {


                for ($j = $i + 1; $j <= $count; $j++) {
                    if (preg_match('#^([a-z0-9\s.:,_-]{2,})$#i', $this['summary'][$j])) {
                        $lines[] = $this['summary'][$j];
                    } else {
                        break;
                    }
                }

                break;
            }
        }

        return $lines;
    }

}
